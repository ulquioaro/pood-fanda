package com.example.poodfanda.payload;

import lombok.Data;

@Data
public class SubcategoriiRequest implements ItemComponent{
    private String name;
    private String descriere;
    private int pret;

    public SubcategoriiRequest() {
    }

    public SubcategoriiRequest(String name, String descriere, int pret) {
        this.name = name;
        this.descriere = descriere;
        this.pret = pret;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }
}
