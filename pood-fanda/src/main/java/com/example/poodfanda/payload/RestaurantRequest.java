package com.example.poodfanda.payload;

import lombok.Data;

@Data
public class RestaurantRequest {
    private String name;

    public RestaurantRequest(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
