package com.example.poodfanda.payload;

import lombok.Data;

@Data
public class MeniuRequest {
    private String name;

    public MeniuRequest(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
