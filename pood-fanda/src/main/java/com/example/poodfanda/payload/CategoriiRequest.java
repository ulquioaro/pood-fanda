package com.example.poodfanda.payload;

import lombok.Data;

@Data
public class CategoriiRequest {
    private String name;

    public CategoriiRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
