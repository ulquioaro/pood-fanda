package com.example.poodfanda.maper;

import com.example.poodfanda.model.Categorii;
import com.example.poodfanda.payload.CategoriiRequest;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper
public interface CategoriiMapper {
    List<CategoriiRequest> toCatReq(List<Categorii> categoriis);
    Set<CategoriiRequest> toCatReqSet(Set<Categorii> categoriiSet);
    Categorii toCat(CategoriiRequest categoriiRequest);
}
