package com.example.poodfanda.maper;

import com.example.poodfanda.model.Restaurant;
import com.example.poodfanda.payload.RestaurantRequest;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface RestaurantMapper {
    RestaurantRequest toRestReq(Restaurant restaurant);
    Restaurant toRest(RestaurantRequest restaurantRequest);
}
