package com.example.poodfanda.maper;

import com.example.poodfanda.model.Subcategorii;
import com.example.poodfanda.payload.SubcategoriiRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


import java.util.List;
import java.util.Set;

@Mapper
public interface SubcategoriiMapper {
    List<SubcategoriiRequest> toSubcReqs(List<Subcategorii> subc);
    Set<SubcategoriiRequest> toSubcReqSet(Set<Subcategorii> subc);
    @Mapping(source = "name", target = "name")
    Subcategorii toSub(SubcategoriiRequest subcategoriiRequest);
}
