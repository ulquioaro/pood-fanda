package com.example.poodfanda.maper;

import com.example.poodfanda.Repository.MeniuRepository;
import com.example.poodfanda.model.Meniu;
import com.example.poodfanda.payload.MeniuRequest;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface MeniuMapper {
    Meniu toMeniu(MeniuRequest meniuRequest);
    MeniuRequest toMeniuReq(Meniu M);
}
