package com.example.poodfanda.maper;

import com.example.poodfanda.model.User;
import com.example.poodfanda.payload.SignUpRequest;
import com.example.poodfanda.payload.UserSummary;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    List<UserSummary> toUserSummary(List<User>  users);
    User toUser(SignUpRequest signUpRequest);
}
