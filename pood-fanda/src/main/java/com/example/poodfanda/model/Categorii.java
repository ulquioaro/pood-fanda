package com.example.poodfanda.model;

import com.example.poodfanda.model.audit.DateAudit;

import javax.persistence.*;


import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="categorii")
public class Categorii extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 40)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "categorii_subcategorii",
            joinColumns = @JoinColumn(name = "categorii_id"),
            inverseJoinColumns = @JoinColumn(name = "subcategorii_id"))
    private Set<Subcategorii> subs = new HashSet<>();

    public Categorii() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Subcategorii> getSubs() {
        return subs;
    }

    public void setSubs(Set<Subcategorii> subs) {
        this.subs = subs;
    }

    public boolean adaugaElement(Subcategorii s){
        return this.subs.add(s);
    }

    public boolean eliminaElement(Subcategorii s){
        return this.subs.remove(s);
    }

}
