package com.example.poodfanda.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}