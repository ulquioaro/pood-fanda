package com.example.poodfanda.model;

import com.example.poodfanda.model.audit.DateAudit;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="meniu")
public class Meniu extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 40)
    private String name;

    @OneToOne(cascade = CascadeType.ALL,optional = false)
    @JoinColumn(name = "restaurant_id",nullable = false)
    private Restaurant restaurant;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "meniu_scategorii",
            joinColumns = @JoinColumn(name = "meniu_id"),
            inverseJoinColumns = @JoinColumn(name = "categorii_id"))
    private Set<Categorii> categorii = new HashSet<>();

    public Meniu() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Categorii> getCategorii() {
        return categorii;
    }

    public void setCategorii(Set<Categorii> categorii) {
        this.categorii = categorii;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public boolean adaugaElement(Categorii s){
        return this.categorii.add(s);
    }

    public boolean eliminaElement(Categorii s){
        return this.categorii.remove(s);
    }
}
