package com.example.poodfanda.service;

import com.example.poodfanda.Repository.MeniuRepository;
import com.example.poodfanda.exception.BadRequestException;
import com.example.poodfanda.model.Meniu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MeniuService {
    @Autowired
    private MeniuRepository meniuRepository;

    public Meniu save(Meniu m){
        return meniuRepository.save(m);
    }

    public Meniu findByName(String name){
        return meniuRepository.findByName(name).orElseThrow(()->new BadRequestException("Item-ul nu exista!"));
    }

}
