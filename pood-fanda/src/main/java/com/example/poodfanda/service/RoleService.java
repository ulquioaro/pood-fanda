package com.example.poodfanda.service;

import com.example.poodfanda.Repository.RoleRepository;
import com.example.poodfanda.exception.BadRequestException;
import com.example.poodfanda.model.Role;
import com.example.poodfanda.model.RoleName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    public Role findByName(RoleName roleName){
        return roleRepository.findByName(roleName).orElseThrow(()->new BadRequestException("Role not found!"));
    }
}
