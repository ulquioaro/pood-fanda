package com.example.poodfanda.service;

import com.example.poodfanda.Repository.CategoriiRepository;
import com.example.poodfanda.exception.BadRequestException;
import com.example.poodfanda.model.Categorii;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriiService {
    @Autowired
    private CategoriiRepository categoriiRepository;

    public Categorii save(Categorii categorii){
        return categoriiRepository.save(categorii);
    }

    public List<Categorii> findAll(){
        return categoriiRepository.findAll();
    }

    public Categorii findByName(String name){
        return categoriiRepository.findByName(name).orElseThrow(()->new BadRequestException("Categoria nu exista!"));
    }

    public void deleteByName(String name){
        Categorii c=categoriiRepository.findByName(name).orElseThrow(()->new BadRequestException("Categoria nu exista!"));
        categoriiRepository.deleteById(c.getId());
    }

}
