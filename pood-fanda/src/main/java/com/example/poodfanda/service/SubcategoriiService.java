package com.example.poodfanda.service;

import com.example.poodfanda.Repository.SubcategoriiRepository;
import com.example.poodfanda.exception.BadRequestException;
import com.example.poodfanda.model.Subcategorii;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubcategoriiService {
    @Autowired
    private SubcategoriiRepository subcategoriiRepository;

    public Subcategorii save(Subcategorii subcategorii){
       return subcategoriiRepository.save(subcategorii);
    }

    public List<Subcategorii> findAll(){
        return subcategoriiRepository.findAll();
    }

    public Subcategorii findByName(String name){
        return subcategoriiRepository.findByName(name).orElseThrow(()->new BadRequestException("Item-ul nu exista!"));
    }

    public void deleteSubcategorii(String nume){
        Subcategorii subc=subcategoriiRepository.findByName(nume)
                .orElseThrow(()->new BadRequestException("Item-ul nu exista!"));
       // System.out.println(subc.getId()+"aici uitate");
        subcategoriiRepository.deleteById(subc.getId());
    }
}
