package com.example.poodfanda.service;

import com.example.poodfanda.Repository.RestaurantRepository;
import com.example.poodfanda.exception.BadRequestException;
import com.example.poodfanda.model.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantService {
    @Autowired
    private RestaurantRepository restaurantRepository;

    public Restaurant save(Restaurant restaurant){
        return restaurantRepository.save(restaurant);
    }

    public Restaurant findByName(String name){
        return restaurantRepository.findByName(name).orElseThrow(()->new BadRequestException("Item-ul nu exista!"));
    }
}
