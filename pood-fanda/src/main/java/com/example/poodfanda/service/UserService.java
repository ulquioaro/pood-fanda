package com.example.poodfanda.service;

import com.example.poodfanda.Repository.UserRepository;
import com.example.poodfanda.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public void deleteByEmail(String email){
        userRepository.deleteByEmail(email);
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public User save(User user){
       return userRepository.save(user);
    }

}
