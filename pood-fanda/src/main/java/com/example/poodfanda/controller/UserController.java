package com.example.poodfanda.controller;

import com.example.poodfanda.Repository.UserRepository;
import com.example.poodfanda.maper.UserMapper;
import com.example.poodfanda.payload.UserSummary;
import com.example.poodfanda.security.CurrentUser;
import com.example.poodfanda.security.UserPrincipal;
import com.example.poodfanda.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName(),currentUser.getEmail());
        System.out.println(currentUser.getAuthorities().size());
        return userSummary;
    }

   @PostMapping("/user/delUser")
   @PreAuthorize("hasRole('Admin')")
    public ResponseEntity deleteByEmail(@RequestBody String email){
        userService.deleteByEmail(email);
       return ResponseEntity.status(HttpStatus.ACCEPTED).build();
   }




}
