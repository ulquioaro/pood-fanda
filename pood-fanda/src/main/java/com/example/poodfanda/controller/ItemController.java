package com.example.poodfanda.controller;

import com.example.poodfanda.maper.CategoriiMapper;
import com.example.poodfanda.maper.MeniuMapper;
import com.example.poodfanda.maper.RestaurantMapper;
import com.example.poodfanda.maper.SubcategoriiMapper;
import com.example.poodfanda.model.Categorii;
import com.example.poodfanda.model.Meniu;
import com.example.poodfanda.model.Restaurant;
import com.example.poodfanda.model.Subcategorii;
import com.example.poodfanda.payload.*;
import com.example.poodfanda.security.CurrentUser;
import com.example.poodfanda.security.UserPrincipal;
import com.example.poodfanda.service.CategoriiService;
import com.example.poodfanda.service.MeniuService;
import com.example.poodfanda.service.RestaurantService;
import com.example.poodfanda.service.SubcategoriiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class ItemController {

    @Autowired
    private SubcategoriiService subcategoriiService;

    @Autowired
    private SubcategoriiMapper subcategoriiMapper;

    @Autowired
    private CategoriiMapper categoriiMapper;

    @Autowired
    private CategoriiService categoriiService;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private RestaurantMapper restaurantMapper;

    @Autowired
    private MeniuMapper meniuMapper;

    @Autowired
    private MeniuService meniuService;

    @PostMapping("/restaurant={restaurant}/meniu/categoriecategorie={categorie}/addSub")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity addSubcategorie(@Valid @RequestBody SubcategoriiRequest subcategoriiRequest,
                                          @PathVariable(value = "categorie") String categorie,
                                          @PathVariable(value = "restaurant") String restaurant){
        Subcategorii subcategorii=subcategoriiMapper.toSub(subcategoriiRequest);
        subcategorii.setCreatedAt(Instant.now());
        subcategorii.setUpdatedAt(Instant.now());

        return ResponseEntity.ok(subcategoriiService.save(subcategorii));
    }

    @GetMapping("/restaurant={restaurant}/meniu/categorie={categorie}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Set<SubcategoriiRequest>> getAllSubcategorii(@PathVariable(value = "categorie") String categorie,@PathVariable(value = "restaurant") String restaurant){
        //Restaurant restaurant1=restaurantService.findByName(restaurant);
        //Meniu meniu=restaurant1.getMeniu();

        Categorii categorii=categoriiService.findByName(categorie);

        return ResponseEntity.ok(subcategoriiMapper.toSubcReqSet(categorii.getSubs()));
    }

    @PostMapping("/restaurant={restaurant}/meniu/categorie={categorie}/subc_update")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity modSubcat(@Valid @RequestBody SubcategoriiRequest subcategoriiRequest,
                                    @PathVariable(value = "categorie") String categorie,@PathVariable(value = "restaurant") String restaurant){
        Subcategorii subcategorii=subcategoriiService.findByName(subcategoriiRequest.getName());
        Subcategorii rez=subcategoriiMapper.toSub(subcategoriiRequest);
        rez.setCreatedAt(subcategorii.getCreatedAt());
        rez.setUpdatedAt(Instant.now());
        rez.setId(subcategorii.getId());
        return ResponseEntity.status(HttpStatus.CREATED).body(subcategoriiService.save(rez));
    }

    @PostMapping("/restaurant={restaurant}/meniu/categorie={categorie}/delSubcategorie")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity delSubc(@Valid @RequestBody SubcategoriiRequest subcategoriiRequest,
                                  @PathVariable(value = "categorie") String categorie,@PathVariable(value = "restaurant") String restaurant){
        subcategoriiService.deleteSubcategorii(subcategoriiRequest.getName());

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @PostMapping("/restaurant={restaurant}/meniu/categorii/addCatDatabase")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity addCategorie(@Valid @RequestBody CategoriiRequest categoriiRequest,@PathVariable(value = "restaurant") String restaurant){
        Categorii categorii=categoriiMapper.toCat(categoriiRequest);
        categorii.setCreatedAt(Instant.now());
        categorii.setUpdatedAt(Instant.now());

        return ResponseEntity.ok(categoriiService.save(categorii));
    }

    @GetMapping("/restaurant={restaurant}/meniu")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Set<CategoriiRequest>> getAllCategorii(@CurrentUser UserPrincipal currentUser,@PathVariable(value = "restaurant") String restaurant){
        Restaurant restaurant1=restaurantService.findByName(restaurant);
        return  ResponseEntity.ok(categoriiMapper.toCatReqSet(restaurant1.getMeniu().getCategorii()));
    }

    @PostMapping("/restaurant={restaurant}/meniu/categorie={categorie}/adaugaItem")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity adaugaItem(@Valid @RequestBody SubcategoriiRequest subcategoriiRequest,@PathVariable(value = "categorie") String name,@PathVariable(value = "restaurant") String restaurant){

        Subcategorii subcategorii=subcategoriiService.findByName(subcategoriiRequest.getName());
        Categorii categorii=categoriiService.findByName(name);
        categorii.adaugaElement(subcategorii);

        return ResponseEntity.status(HttpStatus.CREATED).body( categoriiService.save(categorii));
    }

    @PostMapping("/restaurant/meniu/categorie={categorie}/eliminaItem")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity eliminaItem(@Valid @RequestBody SubcategoriiRequest subcategoriiRequest,@PathVariable(value = "categorie") String name){

        Subcategorii subcategorii=subcategoriiService.findByName(subcategoriiRequest.getName());
        Categorii categorii=categoriiService.findByName(name);
        categorii.adaugaElement(subcategorii);

        return ResponseEntity.status(HttpStatus.CREATED).body( categoriiService.save(categorii));
    }

    @PostMapping("/addRest")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity addRestaurant(@Valid @RequestBody RestaurantRequest restaurantRequest){

        return ResponseEntity.ok(restaurantService.save(restaurantMapper.toRest(restaurantRequest)));
    }

    @PostMapping("/restaurant={restaurant}/addMeniu")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity addMeniu(@Valid @RequestBody MeniuRequest meniuRequest,@PathVariable(value = "restaurant") String name){
        Meniu meniu=meniuMapper.toMeniu(meniuRequest);
        meniu.setCreatedAt(Instant.now());
        meniu.setUpdatedAt(Instant.now());
        Restaurant restaurant=restaurantService.findByName(name);
        restaurant.setMeniu(meniu);
        meniu.setRestaurant(restaurant);

        return ResponseEntity.status(HttpStatus.CREATED).body(restaurantService.save(restaurant));
    }

    @GetMapping("/restaurant={restaurant}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Meniu> vizualizeazaMeniu(@CurrentUser UserPrincipal currentUser,@PathVariable(value = "restaurant") String name){
        Restaurant restaurant=restaurantService.findByName(name);
        return ResponseEntity.ok(restaurant.getMeniu());
    }

    @PostMapping("/restaurant={restaurant}/meniu/addCat")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity addMeniuCat(@Valid @RequestBody CategoriiRequest categoriiRequest,@PathVariable(value = "restaurant") String name){
        Restaurant restaurant=restaurantService.findByName(name);
        Meniu meniu=restaurant.getMeniu();
        meniu.adaugaElement(categoriiService.findByName(categoriiRequest.getName()));
        return ResponseEntity.status(HttpStatus.CREATED).body(meniuService.save(meniu));
    }
}
