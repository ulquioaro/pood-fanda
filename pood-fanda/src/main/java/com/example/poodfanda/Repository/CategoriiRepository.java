package com.example.poodfanda.Repository;

import com.example.poodfanda.model.Categorii;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CategoriiRepository extends JpaRepository<Categorii,Long> {

    List<Categorii> findAll();

    @Override
    Optional<Categorii> findById(Long aLong);

    Optional<Categorii> findByName(String name);
}
