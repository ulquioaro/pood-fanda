package com.example.poodfanda.Repository;

import com.example.poodfanda.model.Meniu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MeniuRepository extends JpaRepository<Meniu,Long> {
    Optional<Meniu> findByName(String name);

}
