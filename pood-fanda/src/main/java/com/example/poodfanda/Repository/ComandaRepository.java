package com.example.poodfanda.Repository;

import com.example.poodfanda.model.Comanda;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComandaRepository extends JpaRepository<Comanda,Long> {
}
