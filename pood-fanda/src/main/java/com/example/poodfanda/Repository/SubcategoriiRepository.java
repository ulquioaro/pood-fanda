package com.example.poodfanda.Repository;

import com.example.poodfanda.model.Subcategorii;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SubcategoriiRepository extends JpaRepository<Subcategorii,Long> {
    @Override
    Optional<Subcategorii> findById(Long aLong);

    Optional<Subcategorii> findByName(String name);

    List<Subcategorii> findAll();

}
